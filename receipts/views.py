from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list_view(request):
    model_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": model_list}
    return render(request, "receipts/receiptlistview.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def expense_category_list_view(request):
    model_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_list": model_list}
    return render(request, "receipts/expensecategorylist.html", context)


@login_required
def account_list_view(request):
    model_list = Account.objects.filter(owner=request.user)
    context = {"account_list": model_list}
    return render(request, "receipts/accountslist.html", context)


@login_required
def expense_category_create_view(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save(commit=False)
            model_instance.owner = request.user
            model_instance.save()
            return redirect("category_list")

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "receipts/expensecategorycreateview.html", context)


@login_required
def account_create_view(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save(commit=False)
            model_instance.owner = request.user
            model_instance.save()
            return redirect("account_list")

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "receipts/accountcreateview.html", context)
