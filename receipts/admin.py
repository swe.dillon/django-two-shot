from django.contrib import admin
from .models import Account, Receipt, ExpenseCategory

# Register your models here.

admin.site.register(Account)
admin.site.register(Receipt)
admin.site.register(ExpenseCategory)


class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "account",
        "category",
        "id",
    )
