from django.urls import path
from receipts.views import (
    receipt_list_view,
    create_receipt,
    expense_category_list_view,
    account_list_view,
    expense_category_create_view,
    account_create_view,
)

urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_category_list_view, name="category_list"),
    path("accounts/", account_list_view, name="account_list"),
    path(
        "categories/create/",
        expense_category_create_view,
        name="create_category",
    ),
    path("accounts/create/", account_create_view, name="create_account"),
]
